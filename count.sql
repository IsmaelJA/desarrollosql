/* COUNT
Solicita las columnas department_id, job_id y cuenta cuantos registros hay 
en cada departamento, condicionado por WHERE que filtra a los departamentos que 
su department_id sea mayor a 40
*/
SELECT department_id, job_id, count(*)
FROM employees
WHERE department_id > 40
GROUP BY department_id, job_id;